# ck-macros

Composable Scheme macros based on the CK abstract machine.

- **Version:**           0.4.0 (in development)
- **Docs:**              https://wiki.call-cc.org/eggref/5/ck-macros
- **License:**           Released to the public domain
- **Maintainer:**        John Croisant (john (at) croisant (dot) net)
- **Based on work by:**  Oleg Kiselyov

This library is based on the CK-macro system described in
"[Applicative syntax-rules: macros that compose better](http://okmij.org/ftp/Scheme/macros.html#ck-macros)"
by Oleg Kiselyov.
This library provides an enhanced version of the core `ck` macro,
many useful (and some not-so-useful) portable CK-macros,
the `ck-wrapper` procedure, and many wrappers of R5RS procedures.

This library is the source for the
[ck-macros egg for CHICKEN Scheme](http://wiki.call-cc.org/eggref/5/ck-macros),
but everything in
[portable.scm](https://gitlab.com/jcroisant/ck-macros/blob/master/lib/portable.scm)
should work on any Scheme implementation of R5RS or later], and
[ck-wrapper.scm](https://gitlab.com/jcroisant/ck-macros/blob/master/lib/ck-wrapper.scm)
and [wrappers-r5rs.scm](https://gitlab.com/jcroisant/ck-macros/blob/master/lib/wrappers-r5rs.scm)
should work on any Scheme that provides `er-macro-transformer`.

If you create a useful or interesting general-purpose CK-macro,
or an improvement to an existing CK-macro,
please contact the maintainer (John) so your contribution can be added to the library.
All source code (including contributions) is released by its authors to the public domain.
For more information, please refer to http://unlicense.org .

Please be aware that all project participants are expected to abide by
the [Code of Conduct](CODE_OF_CONDUCT.md).
We are committed to making participation in this project a welcoming
and harassment-free experience.
