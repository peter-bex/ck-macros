
;;; ck-macros
;;; Composable Scheme macros based on the CK abstract machine.
;;; Version 0.3.0 (2019-03-17)
;;;
;;; All source code (including contributions) is released by its
;;; authors to the public domain. For more information, please refer
;;; to <http://unlicense.org>

(module ck-macros *
  (import scheme)
  (cond-expand
    (chicken-4
     (import chicken))
    (chicken-5
     (import (chicken base)
             (chicken syntax))))

  (include "lib/portable.scm")

  (begin-for-syntax
   (cond-expand
     (chicken-4 (import chicken))
     (chicken-5 (import (chicken port)))
     (else))
   (include "lib/ck-wrapper.scm")       ; Import into syntax env, for macro use

   (define %ck:strip-syntax strip-syntax)
   (define %ck:get-line-number get-line-number)
   (include "lib/helpers.scm"))

  (include "lib/ck-wrapper.scm")        ; Import into runtime env, for exporting
  (include "lib/wrappers-r5rs.scm")
  (include "lib/c-error.chicken.scm")
  (include "lib/c-trace.scm"))
