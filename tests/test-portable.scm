;;; Tests for lib/portable.scm.
;;; Depends on a testing API compatible with CHICKEN's test egg.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; QUASIQUOTE

(test-group "quasiquote"
  (test 3 (ck () `(+ 1 2)))
  (test '(+ 1 2) (ck () ``(+ 1 2)))
  (test '(1 2 3) (ck () (c-quasiquote '`(1 2 3))))
  (test '(1 2 3) (ck () (c-quasiquote (c-quasiquote ''(1 2 3)))))

  (test 1 (ck () `1))
  (test 1 (ck () `,'1))

  (test '(1 (2 3) 4) (ck () (c-quote `(,'1 ,(c-list '2 '3) 4))))
  (test '(1 2 3 4) (ck () (c-quote `(,'1 ,@(c-list '2 '3) 4))))

  (test '#(1 (2 3) 4) (ck () (c-quote `#(,'1 ,(c-list '2 '3) 4))))
  (test '#(1 2 3 4) (ck () (c-quote `#(,'1 ,@(c-list '2 '3) 4))))

  ;; Deep unquoting.
  (test '(define (foo) (bar (baz 1 2)))
        (ck () (c-quote `(define (foo) (bar (baz ,@(c-list '1 '2))))))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; GENERAL

(test-group "c-quote"
  (test '(+ 1 2 3) (ck () (c-quote (c-list '+ '1 '2 '3))))
  (test 6          (ck ()          (c-list '+ '1 '2 '3))))


(test-group "c-eval"
  (test '()      (ck () (c-quote (c-eval '(c-list)))))
  (test '(a . b) (ck () (c-quote (c-eval '(c-cons 'a 'b)))))
  (test 'a       (ck () (c-quote (c-eval '(c-or '#f 'a '#f)))))
  (test 'a       (ck () (c-quote (c-eval '(c-eval '(c-eval '(c-identity 'a))))))))


(test-group "c-call"
  (test '()      (ck () (c-quote (c-call '(c-list)))))
  (test '(a . b) (ck () (c-quote (c-call '(c-cons) 'a 'b))))
  (test 'a       (ck () (c-quote (c-call '(c-or) '#f 'a '#f))))
  (test 'a       (ck () (c-quote (c-call '(c-call '(c-call '(c-identity 'a))))))))


(test-group "c-apply"
  (test '() (ck () (c-quote (c-apply '(c-list) '()))))
  (test '(a) (ck () (c-quote (c-apply '(c-list) '(a)))))
  (test '(a b (c) d) (ck () (c-quote (c-apply '(c-list 'a) 'b '(c) '(d)))))
  (test 1 (ck () (c-apply '(c-or) (c-apply '(c-list) '#f '(1 #f))))))


(test-group "c-compose"
  (test 42 (ck () (c-compose '() '42)))
  (test #t (ck () (c-compose '((c-true)))))
  (test 1 (ck () (c-compose '((c-car)) '(1 2 3))))
  (test 2 (ck () (c-compose '((c-car) (c-cdr)) '(1 2 3))))
  (test 3 (ck () (c-compose '((c-car) (c-cdr) (c-cdr)) '(1 2 3))))
  (test '(#f #t #t #f)
        (ck () (c-quote (c-map1 '(c-compose '((c-not) (c-boolean?)))
                                '(#f a 3 #t))))))


(test-group "c-rcompose"
  (test 42 (ck () (c-rcompose '() '42)))
  (test #t (ck () (c-rcompose '((c-true)))))
  (test 1 (ck () (c-rcompose '((c-car)) '(1 2 3))))
  (test 2 (ck () (c-rcompose '((c-cdr) (c-car)) '(1 2 3))))
  (test 3 (ck () (c-rcompose '((c-cdr) (c-cdr) (c-car)) '(1 2 3))))
  (test '(#f #t #t #f)
        (ck () (c-quote (c-map1 '(c-rcompose '((c-boolean?) (c-not)))
                                '(#f a 3 #t))))))

(test-group "c-flip"
  (test '(a c b) (ck () (c-quote (c-flip '(c-list 'a) 'b 'c))))
  (test '((a . 1) (b . 2) (c . 3))
        (ck () (c-quote (c-map2 '(c-flip '(c-cons))
                                '(1 2 3)
                                '(a b c))))))

(test-group "c-branch"
  (test '(a (b) (x a b))
        (ck () (c-quote (c-branch '((c-car) (c-cdr) (c-cons 'x))
                                  '(a b)))))
  (test '((a 1 2 3) (b 1 2 3))
        (ck () (c-quote (c-branch '((c-list 'a) (c-list 'b))
                                  '1 '2 '3)))))


(test-group "c-identity"
  (test 'a   (ck () (c-quote (c-identity 'a))))
  (test '()  (ck () (c-quote (c-identity (c-list)))))
  (test '(a) (ck () (c-quote (c-identity (c-list 'a))))))


(test-group "c-constantly"
  (test 1 (ck () (c-constantly '1)))
  (test 2 (ck () (c-constantly '2 '#t)))
  (test 3 (ck () (c-constantly '3 '#f)))
  (test 42 (ck () (c-constantly '42 '1 '2 '3))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; CK-MACRO BUILDERS

(test-group "c-make-assoc"
  (ck () `(define-syntax %assoc1
            ,(c-make-assoc '%assoc1 '(a b c) '(a b c 1 2 3))))

  (test '(b . "b")
        (ck () (c-quote (%assoc1 'b '((z . "z") (b . "b") (3 . "3"))))))
  (test '(3 . "3")
        (ck () (c-quote (%assoc1 '3 '((z . "z") (b . "b") (3 . "3"))))))
  (test #f
        (ck () (c-quote (%assoc1 'c '((z . "z") (b . "b") (3 . "3"))))))

  (test '(a . "a") (ck () (c-quote (%assoc1 'a '((z . "z") (a . "a"))))))
  (test #f (ck () (c-quote (%assoc1 'a '()))))
  ;; z is not one of the possible keys
  (test #f (ck () (c-quote (%assoc1 'z '((z . "z") (a . "a"))))))

  ;; x is not a literal, so it will match any value
  (ck () `(define-syntax %assoc-pat
            ,(c-make-assoc '%assoc-pat '(a) '((a x) (x 1)))))

  (test '((a 1) . "a1")
        (ck () (c-quote (%assoc-pat '(a 9) '(((a 1) . "a1"))))))
  (test '((a 1) . "a1")
        (ck () (c-quote (%assoc-pat '(b 1) '(((a 1) . "a1"))))))
  (test #f
        (ck () (c-quote (%assoc-pat '(b 9) '(((a 1) . "a1")))))))


(test-group "c-make-eq?"
  (ck () `(define-syntax %eq1
            ,(c-make-eq? '(a b) '(a b "c" (a #(b)) 1 2))))

  (test #t (ck () (%eq1 'a 'a)))
  (test #t (ck () (%eq1 'b 'b)))
  (test #t (ck () (%eq1 '"c" '"c")))
  (test #t (ck () (%eq1 '(a #(b)) '(a #(b)))))
  (test #t (ck () (%eq1 '1 '1)))
  (test #t (ck () (%eq1 '2 '2)))

  (test #f (ck () (%eq1 'a 'b)))
  (test #f (ck () (%eq1 'b 'a)))
  (test #f (ck () (%eq1 'a '(a #(b)))))
  (test #f (ck () (%eq1 '(a #(b)) '(a (b)))))

  (test #f (ck () (%eq1 'z 'z)))
  (test #f (ck () (%eq1 '9 '9))))


(test-group "c-make-rules"
  ;; %matcher1 with literal symbols in expressions.
  (ck () (c-list 'define-syntax '%matcher1
                 (c-make-rules '()
                   '(('1) 'a)
                   '(('2) 'b)
                   '(('3) 'c))))

  (test 'a (ck () (c-quote (%matcher1 '1))))
  (test 'b (ck () (c-quote (%matcher1 '2))))
  (test 'c (ck () (c-quote (%matcher1 '3))))

  ;; %matcher2 with literals (a b c).
  (ck () (c-list 'define-syntax '%matcher2
                 (c-make-rules '(a b c)
                   '(('a) '1)
                   '(('b) '2)
                   '(('c) '3))))

  (test 1 (ck () (%matcher2 'a)))
  (test 2 (ck () (%matcher2 'b)))
  (test 3 (ck () (%matcher2 'c)))

  ;; %matcher3 with complex patterns.
  (ck () (c-list 'define-syntax '%matcher3
                 (c-make-rules '()
                   '(('#(a))
                     '(1 a))
                   '(('#(a b))
                     '(2 b a))
                   '(('z)
                     (c-cons '9 'z)))))

  (test '(1 42) (ck () (c-quote (%matcher3 '#(42)))))
  (test '(2 42 13) (ck () (c-quote (%matcher3 '#(13 42)))))
  (test '(9 . 1) (ck () (c-quote (%matcher3 '1))))
  (test '(9 1 2 3) (ck () (c-quote (%matcher3 '(1 2 3)))))

  ;; %flip-flop mixes literals and non-literals.
  (ck () (c-list 'define-syntax '%flip-flop
                 (c-make-rules '(flip flop)
                   '(('flip 'x 'y)
                     (c-list 'y 'x))
                   '(('flop 'x 'y)
                     '#f))))

  (test '(2 1)
        (ck () (c-quote (%flip-flop 'flip '1 '2))))
  (test #f
        (ck () (c-quote (%flip-flop 'flop '1 '2))))

  ;; %catchall uses a non-list pattern to catch all args.
  (ck () (c-list 'define-syntax '%catchall
                 (c-make-rules '()
                   '(all-args
                     (c-reverse (c-list . all-args))))))

  (test '() (ck () (c-quote (%catchall))))
  (test '(3 2 1) (ck () (c-quote (%catchall '1 '2 '3))))


  ;; %dots uses ... in a pattern and template.
  (ck () (c-list 'define-syntax '%dots
                 (c-make-rules '()
                   '((x y ...)
                     (c-list y ... x)))))
  (test '(1) (ck () (c-quote (%dots '1))))
  (test '(2 3 4 1) (ck () (c-quote (%dots '1 '2 '3 '4)))))


(test-group "c-make-next"
  ;; Define %c-next-square
  (ck () (c-list 'define-syntax '%c-next-square
                 (c-make-next '(0 1 4 9 16 25 36 49 64))))

  (test 1 (ck () (%c-next-square '0)))
  (test 25 (ck () (%c-next-square '16)))
  (test 64 (ck () (%c-next-square '49)))
  (test #f (ck () (%c-next-square '64)))
  (test #f (ck () (%c-next-square '100)))

  ;; Define %c-next-letter
  (ck () (c-list 'define-syntax '%c-next-letter
                 (c-make-next '(#\a #\b #\c #\d))))

  (test #\b (ck () (%c-next-letter '#\a)))
  (test #\d (ck () (%c-next-letter '#\c)))
  (test #f (ck () (%c-next-letter '#\d)))
  (test #f (ck () (%c-next-letter '#\z))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; BOOLEAN LOGIC

(test-group "c-not"
  (test #t (ck () (c-not '#f)))
  (test #f (ck () (c-not '#t)))
  (test #t (ck () (c-not (c-not '#t))))
  (test #f (ck () (c-not 'x)))
  (test #f (ck () (c-not (c-list 'x))))
  (test #f (ck () (c-not (c-list)))))


(test-group "c-true"
  (test #t (ck () (c-true)))
  (test #t (ck () (c-true '#t)))
  (test #t (ck () (c-true '#f)))
  (test #t (ck () (c-true '1 '2 '3))))


(test-group "c-false"
  (test #f (ck () (c-false)))
  (test #f (ck () (c-false '#t)))
  (test #f (ck () (c-false '#f)))
  (test #f (ck () (c-false '1 '2 '3))))


(test-group "c-if"
  (test 'boo
        (ck () (c-quote (c-if '#f 'yay 'boo))))
  (test 'yay
        (ck () (c-quote (c-if '#t 'yay 'boo))))
  (test 'yay
        (ck () (c-quote (c-if 'x 'yay 'boo))))
  (test '(boo)
        (ck () (c-quote (c-if (c-if '#f '#t '#f)
                              (c-list 'yay)
                              (c-list 'boo)))))
  (test '(yay)
        (ck () (c-quote (c-if (c-if '#t '#t '#f)
                              (c-list 'yay)
                              (c-list 'boo)))))
  (test '(yay)
        (ck () (c-quote (c-if (c-if 'x '#t '#f)
                              (c-list 'yay)
                              (c-list 'boo))))))


(test-group "c-if*"
  (test 'boo
        (ck () (c-quote (c-if* '#f ''yay ''boo))))
  (test 'yay
        (ck () (c-quote (c-if* '#t ''yay ''boo))))
  (test 'yay
        (ck () (c-quote (c-if* 'x ''yay ''boo))))
  (test '(boo)
        (ck () (c-quote (c-if* (c-if* '#f ''#t ''#f)
                               '(c-list 'yay)
                               '(c-list 'boo)))))
  (test '(yay)
        (ck () (c-quote (c-if* (c-if* '#t ''#t ''#f)
                               '(c-list 'yay)
                               '(c-list 'boo)))))
  (test '(yay)
        (ck () (c-quote (c-if* (c-if* 'x ''#t ''#f)
                               '(c-list 'yay)
                               '(c-list 'boo)))))

  ;; c-if* only expands whichever branch is needed. If it expanded
  ;; both, these tests would error because c-foobar is not defined.
  (test '(yay)
        (ck () (c-quote (c-if* '#t '(c-list 'yay) '(c-foobar)))))
  (test '(boo)
        (ck () (c-quote (c-if* '#f '(c-foobar) '(c-list 'boo))))))


(test-group "c-or"
  (test #f (ck () (c-quote (c-or))))
  (test #f (ck () (c-quote (c-or '#f))))
  (test 'a (ck () (c-quote (c-or 'a))))
  (test #f (ck () (c-quote (c-or '#f (c-not '#t) (c-not (c-list 'a))))))
  (test '(a) (ck () (c-quote (c-or '#f (c-not '#t) (c-list 'a))))))


(test-group "c-or*"
  (test #f (ck () (c-quote (c-or*))))
  (test #f (ck () (c-quote (c-or* ''#f))))
  (test 'a (ck () (c-quote (c-or* ''a))))
  (test #f (ck () (c-quote (c-or* ''#f '(c-not '#t) '(c-not (c-list 'a))))))
  (test '(a) (ck () (c-quote (c-or* ''#f '(c-not '#t) '(c-list 'a)))))

  ;; c-or* only expands args as needed. If it expanded all args, this
  ;; test would error because c-foobar is not defined.
  (test 'a (ck () (c-quote (c-or* ''a '(c-foobar))))))


(test-group "c-and"
  (test #t (ck () (c-quote (c-and))))
  (test #f (ck () (c-quote (c-and '#f))))
  (test 'a (ck () (c-quote (c-and 'a))))
  (test 'b (ck () (c-quote (c-and 'a 'b))))
  (test #f (ck () (c-quote (c-and 'a (c-not '#t)))))
  (test #f (ck () (c-quote (c-and '#t (c-not '#t) (c-not (c-list 'a)))))))


(test-group "c-and*"
  (test #t (ck () (c-quote (c-and*))))
  (test #f (ck () (c-quote (c-and* ''#f))))
  (test 'a (ck () (c-quote (c-and* ''a))))
  (test 'b (ck () (c-quote (c-and* ''a ''b))))
  (test #f (ck () (c-quote (c-and* ''a '(c-not '#t)))))
  (test #f (ck () (c-quote (c-and* ''#t '(c-not '#t) '(c-not (c-list 'a))))))

  ;; c-and* only expands args as needed. If it expanded all args,
  ;; this test would error because c-foobar is not defined.
  (test #f (ck () (c-quote (c-and* ''#f '(c-foobar))))))


(test-group "c-null?"
  (test #t (ck () (c-null? '())))
  (test #t (ck () (c-null? (c-list))))
  (test #f (ck () (c-null? '(x))))
  (test #f (ck () (c-null? '(()))))
  (test #f (ck () (c-null? '#t)))
  (test #f (ck () (c-null? '#f)))
  (test #f (ck () (c-null? 'x))))


(test-group "c-pair?"
  (test #t (ck () (c-pair? '(x))))
  (test #t (ck () (c-pair? '(x y z))))
  (test #t (ck () (c-pair? (c-cons 'x 'y))))
  (test #t (ck () (c-pair? '(x y . z))))
  (test #f (ck () (c-pair? '())))
  (test #f (ck () (c-pair? (c-list))))
  (test #f (ck () (c-pair? '#t)))
  (test #f (ck () (c-pair? '#f)))
  (test #f (ck () (c-pair? 'x))))


(test-group "c-not-pair?"
  (test #f (ck () (c-not-pair? '(x))))
  (test #f (ck () (c-not-pair? '(x y z))))
  (test #f (ck () (c-not-pair? (c-cons 'x 'y))))
  (test #f (ck () (c-not-pair? '(x y . z))))
  (test #t (ck () (c-not-pair? '())))
  (test #t (ck () (c-not-pair? (c-list))))
  (test #t (ck () (c-not-pair? '#t)))
  (test #t (ck () (c-not-pair? '#f)))
  (test #t (ck () (c-not-pair? 'x))))


(test-group "c-vector?"
  (test #t (ck () (c-vector? '#())))
  (test #t (ck () (c-vector? '#(a))))
  (test #t (ck () (c-vector? '#(a b))))
  (test #f (ck () (c-vector? '())))
  (test #f (ck () (c-vector? '#t)))
  (test #f (ck () (c-vector? '#f)))
  (test #f (ck () (c-vector? 'x))))


(test-group "c-boolean?"
  (test #t (ck () (c-boolean? '#t)))
  (test #t (ck () (c-boolean? '#f)))
  (test #t (ck () (c-boolean? (c-not '#t))))
  (test #t (ck () (c-boolean? (c-not 'a))))
  (test #f (ck () (c-boolean? 'a)))
  (test #f (ck () (c-boolean? '#:a)))
  (test #f (ck () (c-boolean? '1)))
  (test #f (ck () (c-boolean? '"foo")))
  (test #f (ck () (c-boolean? '(#t)))))


(test-group "c-sym-eq?"
  (test #t (ck () (c-sym-eq? 'a 'a)))
  (test #f (ck () (c-sym-eq? 'a 'x)))
  (test #f (ck () (c-sym-eq? 'a '1)))
  (test #f (ck () (c-sym-eq? 'a '"foo")))
  (test #f (ck () (c-sym-eq? 'a '#\x))))


(test-group "c-sym-equal?"
  (test #t (ck () (c-sym-equal? '() '())))
  (test #f (ck () (c-sym-equal? '() '(a))))
  (test #f (ck () (c-sym-equal? '(a) '())))
  (test #t (ck () (c-sym-equal? '(a) '(a))))
  (test #f (ck () (c-sym-equal? '(a) '(x))))
  (test #t (ck () (c-sym-equal? '(a b) '(a b))))
  (test #f (ck () (c-sym-equal? '(a b) '(x b))))
  (test #f (ck () (c-sym-equal? '(a b) '(a x))))
  (test #t (ck () (c-sym-equal? '(a (b)) '(a (b)))))
  (test #f (ck () (c-sym-equal? '(a (b)) '(a (x)))))
  (test #t (ck () (c-sym-equal? '(a . b) '(a . b))))
  (test #f (ck () (c-sym-equal? '(a . b) '(a . x))))

  (test #t (ck () (c-sym-equal? '#() '#())))
  (test #f (ck () (c-sym-equal? '#() '#(a))))
  (test #f (ck () (c-sym-equal? '#(a) '#())))
  (test #t (ck () (c-sym-equal? '#(a) '#(a))))
  (test #f (ck () (c-sym-equal? '#(a) '#(x))))
  (test #t (ck () (c-sym-equal? '#(a b) '#(a b))))
  (test #f (ck () (c-sym-equal? '#(a b) '#(x b))))
  (test #f (ck () (c-sym-equal? '#(a b) '#(a x))))
  (test #t (ck () (c-sym-equal? '#(a #(b)) '#(a #(b)))))
  (test #f (ck () (c-sym-equal? '#(a #(b)) '#(a #(x)))))

  (test #t (ck () (c-sym-equal? '(a (b . #(c))) '(a (b . #(c))))))
  (test #f (ck () (c-sym-equal? '(a (b . #(c))) '(a (b . #(x))))))
  (test #f (ck () (c-sym-equal? '(a (b . #(c))) '(a (b . (c))))))

  (test #t (ck () (c-sym-equal? 'a 'a)))
  (test #f (ck () (c-sym-equal? 'a 'x))))


;;; See test-compare.scm for tests of c-compare?



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LIST PROCESSING


(test-group "c-cons"
  (test '(a . b) (ck () (c-quote (c-cons 'a 'b))))
  (test 3 (ck () (c-cons '+ '(1 2))))
  (test 3 (ck () (c-cons '+ (c-cons '1 (c-cons '2 '()))))))


(test-group "c-cons*"
  (test '(a . b) (ck () (c-quote (c-cons* 'a 'b))))
  (test '(a b c . d) (ck () (c-quote (c-cons* 'a 'b 'c 'd))))
  (test '(a b c d) (ck () (c-quote (c-cons* 'a 'b 'c '(d)))))
  (test 3 (ck () (c-cons* '+ '1 '(2)))))


(test-group "c-xcons"
  (test '(b . a) (ck () (c-quote (c-xcons 'a 'b))))
  (test 3 (ck () (c-xcons '(1 2) '+))))


(test-group "c-list"
  (test '()
        (ck () (c-quote (c-list))))
  (test '(1)
        (ck () (c-quote (c-list '1))))
  (test '(1 (2 (3 4 5)))
        (ck () (c-quote (c-list '1 (c-list '2 (c-list '3 '4 '5))))))
  (test '(define (foo x) (+ x 1))
        (ck () (c-quote (c-list 'define (c-list 'foo 'x) (c-list '+ 'x '1))))))


(test-group "c-car"
  (test 'a (ck () (c-quote (c-car '(a)))))
  (test 'a (ck () (c-quote (c-car '(a b c)))))
  (test 'a (ck () (c-quote (c-car (c-list 'a 'b 'c))))))


(test-group "c-cdr"
  (test '()    (ck () (c-quote (c-cdr '(a)))))
  (test '(b c) (ck () (c-quote (c-cdr '(a b c)))))
  (test '(b c) (ck () (c-quote (c-cdr (c-list 'a 'b 'c))))))


(test-group "c-first ... c-tenth"
  (test '2  (ck () (c-quote (c-first   (c-list '2 '4 '6 '8 '10 '1 '3 '5 '7 '9 '11)))))
  (test '4  (ck () (c-quote (c-second  (c-list '2 '4 '6 '8 '10 '1 '3 '5 '7 '9 '11)))))
  (test '6  (ck () (c-quote (c-third   (c-list '2 '4 '6 '8 '10 '1 '3 '5 '7 '9 '11)))))
  (test '8  (ck () (c-quote (c-fourth  (c-list '2 '4 '6 '8 '10 '1 '3 '5 '7 '9 '11)))))
  (test '10 (ck () (c-quote (c-fifth   (c-list '2 '4 '6 '8 '10 '1 '3 '5 '7 '9 '11)))))
  (test '1  (ck () (c-quote (c-sixth   (c-list '2 '4 '6 '8 '10 '1 '3 '5 '7 '9 '11)))))
  (test '3  (ck () (c-quote (c-seventh (c-list '2 '4 '6 '8 '10 '1 '3 '5 '7 '9 '11)))))
  (test '5  (ck () (c-quote (c-eighth  (c-list '2 '4 '6 '8 '10 '1 '3 '5 '7 '9 '11)))))
  (test '7  (ck () (c-quote (c-ninth   (c-list '2 '4 '6 '8 '10 '1 '3 '5 '7 '9 '11)))))
  (test '9  (ck () (c-quote (c-tenth   (c-list '2 '4 '6 '8 '10 '1 '3 '5 '7 '9 '11))))))


(test-group "c-last"
  (test 'a (ck () (c-quote (c-last (c-list 'a)))))
  (test 'e (ck () (c-quote (c-last (c-list 'a 'b 'c 'd 'e))))))

(test-group "c-last-pair"
  (test '(a)     (ck () (c-quote (c-last-pair (c-list 'a)))))
  (test '(c)     (ck () (c-quote (c-last-pair (c-list 'a 'b 'c)))))
  (test '(b . c) (ck () (c-quote (c-last-pair (c-cons 'a (c-cons 'b 'c)))))))


(test-group "c-drop1 ... c-drop5"
  (test '(4 6 8 1 3 5 7)
        (ck () (c-quote (c-drop1 (c-list '2 '4 '6 '8 '1 '3 '5 '7)))))
  (test '(6 8 1 3 5 7)
        (ck () (c-quote (c-drop2 (c-list '2 '4 '6 '8 '1 '3 '5 '7)))))
  (test '(8 1 3 5 7)
        (ck () (c-quote (c-drop3 (c-list '2 '4 '6 '8 '1 '3 '5 '7)))))
  (test '(1 3 5 7)
        (ck () (c-quote (c-drop4 (c-list '2 '4 '6 '8 '1 '3 '5 '7)))))
  (test '(3 5 7)
        (ck () (c-quote (c-drop5 (c-list '2 '4 '6 '8 '1 '3 '5 '7))))))


(test-group "c-take1 ... c-take5"
  (test '(2)
        (ck () (c-quote (c-take1 (c-list '2 '4 '6 '8 '1 '3 '5 '7)))))
  (test '(2 4)
        (ck () (c-quote (c-take2 (c-list '2 '4 '6 '8 '1 '3 '5 '7)))))
  (test '(2 4 6)
        (ck () (c-quote (c-take3 (c-list '2 '4 '6 '8 '1 '3 '5 '7)))))
  (test '(2 4 6 8)
        (ck () (c-quote (c-take4 (c-list '2 '4 '6 '8 '1 '3 '5 '7)))))
  (test '(2 4 6 8 1)
        (ck () (c-quote (c-take5 (c-list '2 '4 '6 '8 '1 '3 '5 '7))))))


(test-group "c-reverse"
  (test '()      (ck () (c-quote (c-reverse '()))))
  (test '(1)     (ck () (c-quote (c-reverse '(1)))))
  (test '(3 2 1) (ck () (c-quote (c-reverse '(1 2 3)))))
  (test '(3 2 1) (ck () (c-quote (c-reverse (c-cons '1 (c-list '2 '3)))))))


(test-group "c-prefix"
  (test '(1 2 3 4 5)
        (ck () (c-quote (c-prefix '() '1 '2 '3 '4 '5))))
  (test '(2 3 4 5 1)
        (ck () (c-quote (c-prefix '(1) '2 '3 '4 '5))))
  (test '(4 5 1 2 3)
        (ck () (c-quote (c-prefix '(1 2 3) '4 '5)))))


(test-group "c-suffix"
  (test '(1 2 3 4 5)
        (ck () (c-quote (c-suffix '() '1 '2 '3 '4 '5))))
  (test '(1 2 3 4 5)
        (ck () (c-quote (c-suffix '(1) '2 '3 '4 '5))))
  (test '(1 2 3 4 5)
        (ck () (c-quote (c-suffix '(1 2 3) '4 '5)))))


(test-group "c-append"
  (test '()
        (ck () (c-quote (c-append))))
  (test '(1 2 3)
        (ck () (c-quote (c-append '(1 2 3)))))
  (test '()
        (ck () (c-quote (c-append '() '()))))
  (test '(1 2 3 4 5)
        (ck () (c-quote (c-append '() '(1 2 3 4 5)))))
  (test '(1 2 3 4 5)
        (ck () (c-quote (c-append '(1) '(2 3 4 5)))))
  (test '(1 2 3 4 5)
        (ck () (c-quote (c-append '(1 2 3) '(4 5)))))
  (test '(1 2 3 4 5)
        (ck () (c-quote (c-append '(1) '(2) '(3) '(4) '(5))))))


(test-group "c-append-map1"
  (test '(10 1 10 2 10 3 10 4)
        (ck () (c-quote (c-append-map1 '(c-cons '10) '((1) (2) (3) (4)))))))


(test-group "c-map1"
  (test '((10 1) (10 2) (10 3) (10 4))
        (ck () (c-quote (c-map1 '(c-cons '10) '((1) (2) (3) (4)))))))


(test-group "c-map2"
  (test '((a1 a2) (b1 b2) (c1 c2))
        (ck () (c-quote (c-map2 '(c-list) '(a1 b1 c1) '(a2 b2 c2)))))
  (test '((a1 a2) (b1 b2))
        (ck () (c-quote (c-map2 '(c-list) '(a1 b1) '(a2 b2 c2)))))
  (test '((a1 a2) (b1 b2))
        (ck () (c-quote (c-map2 '(c-list) '(a1 b1 c1) '(a2 b2))))))


(test-group "c-map3"
  (test '((a1 a2 a3) (b1 b2 b3) (c1 c2 c3))
        (ck () (c-quote (c-map3 '(c-list)
                                '(a1 b1 c1) '(a2 b2 c2) '(a3 b3 c3)))))
  (test '((a1 a2 a3) (b1 b2 b3))
        (ck () (c-quote (c-map3 '(c-list)
                                '(a1 b1) '(a2 b2 c2) '(a3 b3 c3)))))
  (test '((a1 a2 a3) (b1 b2 b3))
        (ck () (c-quote (c-map3 '(c-list)
                                '(a1 b1 c1) '(a2 b2) '(a3 b3 c3)))))
  (test '((a1 a2 a3) (b1 b2 b3))
        (ck () (c-quote (c-map3 '(c-list)
                                '(a1 b1 c1) '(a2 b2 c3) '(a3 b3))))))


(test-group "c-map4"
  (test '((a1 a2 a3 a4) (b1 b2 b3 b4) (c1 c2 c3 c4))
        (ck () (c-quote (c-map4 '(c-list)
                                '(a1 b1 c1) '(a2 b2 c2)
                                '(a3 b3 c3) '(a4 b4 c4)))))
  (test '((a1 a2 a3 a4) (b1 b2 b3 b4))
        (ck () (c-quote (c-map4 '(c-list)
                                '(a1 b1) '(a2 b2 c2)
                                '(a3 b3 c3) '(a4 b4 c4)))))
  (test '((a1 a2 a3 a4) (b1 b2 b3 b4))
        (ck () (c-quote (c-map4 '(c-list)
                                '(a1 b1 c1) '(a2 b2)
                                '(a3 b3 c3) '(a4 b4 c4)))))
  (test '((a1 a2 a3 a4) (b1 b2 b3 b4))
        (ck () (c-quote (c-map4 '(c-list)
                                '(a1 b1 c1) '(a2 b2 c2)
                                '(a3 b3) '(a4 b4 c4)))))
  (test '((a1 a2 a3 a4) (b1 b2 b3 b4))
        (ck () (c-quote (c-map4 '(c-list)
                                '(a1 b1 c1) '(a2 b2 c2)
                                '(a3 b3 c3) '(a4 b4))))))

(test-group "c-map5"
  (test '((a1 a2 a3 a4 a5) (b1 b2 b3 b4 b5) (c1 c2 c3 c4 c5))
        (ck () (c-quote (c-map5 '(c-list)
                                '(a1 b1 c1) '(a2 b2 c2) '(a3 b3 c3)
                                '(a4 b4 c4) '(a5 b5 c5)))))
  (test '((a1 a2 a3 a4 a5) (b1 b2 b3 b4 b5))
        (ck () (c-quote (c-map5 '(c-list)
                                '(a1 b1) '(a2 b2 c2) '(a3 b3 c3)
                                '(a4 b4 c4) '(a5 b5 c5)))))
  (test '((a1 a2 a3 a4 a5) (b1 b2 b3 b4 b5))
        (ck () (c-quote (c-map5 '(c-list)
                                '(a1 b1 c1) '(a2 b2) '(a3 b3 c3)
                                '(a4 b4 c4) '(a5 b5 c5)))))
  (test '((a1 a2 a3 a4 a5) (b1 b2 b3 b4 b5))
        (ck () (c-quote (c-map5 '(c-list)
                                '(a1 b1 c1) '(a2 b2 c2) '(a3 b3)
                                '(a4 b4 c4) '(a5 b5 c5)))))
  (test '((a1 a2 a3 a4 a5) (b1 b2 b3 b4 b5))
        (ck () (c-quote (c-map5 '(c-list)
                                '(a1 b1 c1) '(a2 b2 c2) '(a3 b3 c3)
                                '(a4 b4) '(a5 b5 c5)))))
  (test '((a1 a2 a3 a4 a5) (b1 b2 b3 b4 b5))
        (ck () (c-quote (c-map5 '(c-list)
                                '(a1 b1 c1) '(a2 b2 c2) '(a3 b3 c3)
                                '(a4 b4 c4) '(a5 b5))))))


(test-group "c-fold1"
  (test '(d c b a)
        (ck () (c-quote (c-fold1 '(c-cons) '() '(a b c d)))))
  (test '(d c b a)
        (ck () (c-quote (c-fold1 '(c-cons) '() (c-list 'a 'b 'c 'd)))))
  (test '(e f c d a b x y)
        (ck () (c-quote (c-fold1 '(c-append) '(x y)
                                 (c-list (c-list 'a 'b)
                                         (c-list 'c 'd)
                                         (c-list 'e 'f)))))))

;;; See test-unfold.scm for tests of c-unfold


(test-group "c-filter"
  (test '(#t #f #t)
        (ck () (c-quote (c-filter '(c-boolean?)
                                  '(a #t b #f #t c)))))
  (test '(#t #f #t)
        (ck () (c-quote (c-filter '(c-boolean?)
                                  (c-list 'a '#t 'b '#f '#t 'c))))))


(test-group "c-remove"
  (test '(a b c)
        (ck () (c-quote (c-remove '(c-boolean?)
                                  '(a #t b #f #t c)))))
  (test '(a b c)
        (ck () (c-quote (c-remove '(c-boolean?)
                                  (c-list 'a '#t 'b '#f '#t 'c))))))


(test-group "c-find"
  (test '#f (ck () (c-quote (c-find '(c-pair?) (c-list '#t 'a '1)))))
  (test '(a . b)
        (ck () (c-quote (c-find '(c-pair?)
                                (c-list '#t 'a (c-cons 'a 'b) (c-cons 'c 'd)))))))


(test-group "c-find-tail"
  (test '#f (ck () (c-quote (c-find-tail '(c-pair?) (c-list '#t 'a '1)))))
  (test '((a . b) (c . d))
        (ck () (c-quote (c-find-tail '(c-pair?)
                                     (c-list '#t 'a (c-cons 'a 'b) (c-cons 'c 'd)))))))


(test-group "c-member"
  (test #f
        (ck () (c-quote (c-member 'a '()))))
  (test '(b c)
        (ck () (c-quote (c-member 'b (c-list 'a 'b 'c)))))
  (test '(c)
        (ck () (c-quote (c-member 'c '(a b c)))))
  (test '((b (c)) d)
        (ck () (c-quote
                (c-member '(b (c))
                          '(a (b (z)) (b (c)) d)))))
  (test '((b (c)) d)
        (ck () (c-quote
                (c-member '(b (c))
                          (c-list 'a '(b (z)) '(b (c)) 'd)))))

  (test '((x y z) (a b))
        (ck () (c-quote (c-member '(a b c)
                                  '((a) (x y z) (a b))
                                  '(c-u=))))))


(test-group "c-any1"
  (test #f (ck () (c-any1 '(c-pair?) '())))
  (test #f (ck () (c-any1 '(c-pair?) (c-list '1 '2 '3))))
  (test #t (ck () (c-any1 '(c-pair?) (c-list '1 (c-cons 'a 'b) '2))))
  (test '(a . 1)
        (ck () (c-quote (c-any1 '(c-cons 'a) (c-list '1 '2 '3))))))


(test-group "c-every1"
  (test #t (ck () (c-every1 '(c-pair?) '())))
  (test #t (ck () (c-every1 '(c-pair?) (c-list (c-cons 'a 'b) (c-cons 'c 'd)))))
  (test #t (ck () (c-every1 '(c-pair?) (c-list (c-cons 'a 'b)))))
  (test #f (ck () (c-every1 '(c-pair?) (c-list '1 (c-cons 'a 'b) '2))))
  (test #f (ck () (c-every1 '(c-pair?) (c-list '1 '2 '3))))
  (test '(a . 3)
        (ck () (c-quote (c-every1 '(c-cons 'a) (c-list '1 '2 '3))))))


(test-group "c-assoc"
  (test #f (ck () (c-quote (c-assoc 'a '()))))
  (test #f (ck () (c-quote (c-assoc 'a '((x . 1) (y . 2))))))
  (test '(x . 1)
        (ck () (c-quote (c-assoc 'x '((x . 1) (y . 2))))))
  (test '((x) . 1)
        (ck () (c-quote (c-assoc '(x) '(((x) . 1) ((y) . 2))))))
  )


(test-group "c-alist-delete"
  (test '()
        (ck () (c-quote (c-alist-delete 'a '()))))
  (test '()
        (ck () (c-quote (c-alist-delete
                         'the '((the cat meows)
                                (the dog barks)
                                (the bird tweets))))))
  (test '((c . a))
        (ck () (c-quote (c-alist-delete
                         'a '((a . b)
                              (c . a)
                              (a . z))))))
  (test '((b . (a)) (a . z))
        (ck () (c-quote (c-alist-delete
                         '(a) '(((a) . b)
                                (b . (a))
                                (a . z)))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; VECTOR PROCESSING

(test-group "c-vector"
  (test '#()
        (ck () (c-quote (c-vector))))
  (test '#(1)
        (ck () (c-quote (c-vector '1))))
  (test '#(1 #(2 #(3 4 5)))
        (ck () (c-quote (c-vector '1 (c-vector '2 (c-vector '3 '4 '5)))))))


(test-group "c-list->vector"
  (test '#()
        (ck () (c-quote (c-list->vector '()))))
  (test '#(1)
        (ck () (c-quote (c-list->vector '(1)))))
  (test '#(1 (2 3))
        (ck () (c-quote (c-list->vector (c-list '1 (c-list '2 '3)))))))


(test-group "c-vector->list"
  (test '()
        (ck () (c-quote (c-vector->list '#()))))
  (test '(1)
        (ck () (c-quote (c-vector->list '#(1)))))
  (test '(1 #(2 3))
        (ck () (c-quote (c-vector->list (c-vector '1 (c-vector '2 '3)))))))


(test-group "c-vector-reverse"
  (test '#()      (ck () (c-quote (c-vector-reverse '#()))))
  (test '#(1)     (ck () (c-quote (c-vector-reverse '#(1)))))
  (test '#(3 2 1) (ck () (c-quote (c-vector-reverse (c-vector '1 '2 '3))))))


(test-group "c-vector-prefix"
  (test '#(1 2 3 4 5)
        (ck () (c-quote (c-vector-prefix '#() '1 '2 '3 '4 '5))))
  (test '#(2 3 4 5 1)
        (ck () (c-quote (c-vector-prefix '#(1) '2 '3 '4 '5))))
  (test '#(4 5 1 2 3)
        (ck () (c-quote (c-vector-prefix '#(1 2 3) '4 '5)))))


(test-group "c-vector-suffix"
  (test '#(1 2 3 4 5)
        (ck () (c-quote (c-vector-suffix '#() '1 '2 '3 '4 '5))))
  (test '#(1 2 3 4 5)
        (ck () (c-quote (c-vector-suffix '#(1) '2 '3 '4 '5))))
  (test '#(1 2 3 4 5)
        (ck () (c-quote (c-vector-suffix '#(1 2 3) '4 '5)))))


(test-group "c-vector-append"
  (test '#()
        (ck () (c-quote (c-vector-append))))
  (test '#(1)
        (ck () (c-quote (c-vector-append '#(1)))))
  (test '#()
        (ck () (c-quote (c-vector-append '#() '#()))))
  (test '#(1)
        (ck () (c-quote (c-vector-append '#(1) '#()))))
  (test '#(1)
        (ck () (c-quote (c-vector-append '#() '#(1)))))
  (test '#(1 2)
        (ck () (c-quote (c-vector-append '#(1) '#(2)))))
  (test '#(1 2 3 4)
        (ck () (c-quote (c-vector-append (c-vector '1 '2) (c-vector '3 '4)))))
  (test '#(1 2 3 4)
        (ck () (c-quote (c-vector-append '#(1) '#(2) '#(3) '#(4))))))


(test-group "c-vector-map1"
  (test '#()
        (ck () (c-quote (c-vector-map1 '(c-cons 'a) '#()))))
  (test '#((a . 1))
        (ck () (c-quote (c-vector-map1 '(c-cons 'a) '#(1)))))
  (test '#((a . 1) (a . 2) (a . 3))
        (ck () (c-quote (c-vector-map1 '(c-cons 'a) (c-vector '1 '2 '3))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; UNARY MATH

(test-group "c-u="
  (test #t (ck () (c-u= '()  '())))
  (test #f (ck () (c-u= '(a) '())))
  (test #f (ck () (c-u= '()  '(b))))
  (test #t (ck () (c-u= '(a) '(b))))
  (test #t (ck () (c-u= (c-list 'a 'b) (c-list 'c 'd))))
  (test #f (ck () (c-u= (c-list 'a 'b) (c-list 'c 'd 'e))))
  (test #f (ck () (c-u= (c-list 'a 'b 'c) (c-list 'd 'e)))))


(test-group "c-u<"
  (test #f (ck () (c-u< '()  '())))
  (test #f (ck () (c-u< '(a) '())))
  (test #t (ck () (c-u< '()  '(b))))
  (test #f (ck () (c-u< '(a) '(b))))
  (test #f (ck () (c-u< (c-list 'a 'b) (c-list 'c 'd))))
  (test #t (ck () (c-u< (c-list 'a 'b) (c-list 'c 'd 'e))))
  (test #f (ck () (c-u< (c-list 'a 'b 'c) (c-list 'd 'e)))))

(test-group "c-u<="
  (test #t (ck () (c-u<= '()  '())))
  (test #f (ck () (c-u<= '(a) '())))
  (test #t (ck () (c-u<= '()  '(b))))
  (test #t (ck () (c-u<= '(a) '(b))))
  (test #t (ck () (c-u<= (c-list 'a 'b) (c-list 'c 'd))))
  (test #t (ck () (c-u<= (c-list 'a 'b) (c-list 'c 'd 'e))))
  (test #f (ck () (c-u<= (c-list 'a 'b 'c) (c-list 'd 'e)))))


(test-group "c-u>"
  (test #f (ck () (c-u> '()  '())))
  (test #t (ck () (c-u> '(a) '())))
  (test #f (ck () (c-u> '()  '(b))))
  (test #f (ck () (c-u> '(a) '(b))))
  (test #f (ck () (c-u> (c-list 'a 'b) (c-list 'c 'd))))
  (test #f (ck () (c-u> (c-list 'a 'b) (c-list 'c 'd 'e))))
  (test #t (ck () (c-u> (c-list 'a 'b 'c) (c-list 'd 'e)))))

(test-group "c-u>="
  (test #t (ck () (c-u>= '()  '())))
  (test #t (ck () (c-u>= '(a) '())))
  (test #f (ck () (c-u>= '()  '(b))))
  (test #t (ck () (c-u>= '(a) '(b))))
  (test #t (ck () (c-u>= (c-list 'a 'b) (c-list 'c 'd))))
  (test #f (ck () (c-u>= (c-list 'a 'b) (c-list 'c 'd 'e))))
  (test #t (ck () (c-u>= (c-list 'a 'b 'c) (c-list 'd 'e)))))


(test-group "c-uzero?"
  (test #t (ck () (c-uzero? '())))
  (test #f (ck () (c-uzero? '(a))))
  (test #f (ck () (c-uzero? (c-cons 'a (c-list 'b))))))


(test-group "c-ueven?"
  (test #t (ck () (c-ueven? '())))
  (test #f (ck () (c-ueven? '(a))))
  (test #t (ck () (c-ueven? '(a b))))
  (test #f (ck () (c-ueven? '(a b c))))
  (test #t (ck () (c-ueven? (c-cons 'a (c-list 'b))))))


(test-group "c-uodd?"
  (test #f (ck () (c-uodd? '())))
  (test #t (ck () (c-uodd? '(a))))
  (test #f (ck () (c-uodd? '(a b))))
  (test #t (ck () (c-uodd? '(a b c))))
  (test #f (ck () (c-uodd? (c-cons 'a (c-list 'b))))))


(test-group "c-u+"
  (test '(a b)                          ; 2 + 0 = 2
        (ck () (c-quote (c-u+ (c-list 'a 'b) '()))))
  (test '(a b c d e)                    ; 2 + 3 = 5
        (ck () (c-quote (c-u+ (c-list 'a 'b) (c-list 'c 'd 'e))))))


(test-group "c-u-"
  (test '(a b c)                        ; 3 - 0 = 3
        (ck () (c-quote (c-u- (c-list 'a 'b 'c) '()))))
  (test '(c d)                          ; 4 - 2 = 2
        (ck () (c-quote (c-u- (c-list 'a 'b 'c 'd) '(x y)))))
  (test '()                             ; 2 - 3 = 0 (no negatives)
        (ck () (c-quote (c-u- (c-list 'a 'b) (c-list 'x 'y 'z)))))
  (test '()                             ; 0 - 3 = 0 (no negatives)
        (ck () (c-quote (c-u- (c-list) (c-list 'x 'y 'z))))))


(test-group "c-u*"
  (test '()                             ; 0 * 0 = 0
        (ck () (c-quote (c-u* '() '()))))
  (test '()                             ; 0 * 2 = 0
        (ck () (c-quote (c-u* '() '(a b)))))
  (test '()                             ; 2 * 0 = 0
        (ck () (c-quote (c-u* '(a b) '()))))
  (test '(a)                            ; 1 * 1 = 1
        (ck () (c-quote (c-u* '(a) '(x)))))
  (test '(a b a b a b)                  ; 2 * 3 = 6
        (ck () (c-quote (c-u* '(a b) '(x y z)))))
  (test '(a b c a b c)                  ; 3 * 2 = 6
        (ck () (c-quote (c-u* '(a b c) '(x y))))))


(test-group "c-u/"
  (test '(() ())                        ; 0 / 1 = 0 rem 0
        (ck () (c-quote (c-u/ '() '(x)))))
  (test '((a) ())                       ; 1 / 1 = 1 rem 0
        (ck () (c-quote (c-u/ '(a) '(x)))))
  (test '((b a) ())                     ; 2 / 1 = 2 rem 0
        (ck () (c-quote (c-u/ '(a b) '(x)))))
  (test '((g d a) (j k))                ; 11 / 3 = 3 rem 2
        (ck () (c-quote (c-u/ '(a b c d e f g h i j k)
                              '(x y z))))))


(test-group "c-ufactorial"
  ;; 0! = 1
  (test '(u) (ck () (c-quote (c-ufactorial '()))))
  ;; 1! = 1
  (test '(x) (ck () (c-quote (c-ufactorial '(x)))))
  ;; 4! = 24
  (test '(a b c d a b c d a b c d a b c d a b c d a b c d)
        (ck () (c-quote (c-ufactorial '(a b c d))))))


(test-group "c-udrop"
  (test '(a b c)
        (ck () (c-quote (c-udrop (c-list 'a 'b 'c) '()))))
  (test '(c d)
        (ck () (c-quote (c-udrop (c-list 'a 'b 'c 'd) '(x y)))))
  (test '()
        (ck () (c-quote (c-udrop (c-list 'a 'b) (c-list 'x 'y 'z)))))
  (test '()
        (ck () (c-quote (c-udrop (c-list) (c-list 'x 'y 'z))))))


(test-group "c-utake"
  (test '()
        (ck () (c-quote (c-utake (c-list 'a 'b 'c) '()))))
  (test '(a b)
        (ck () (c-quote (c-utake (c-list 'a 'b 'c 'd) '(x y)))))
  (test '(a b)
        (ck () (c-quote (c-utake (c-list 'a 'b) (c-list 'x 'y 'z)))))
  (test '()
        (ck () (c-quote (c-utake (c-list) (c-list 'x 'y 'z))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; DECIMAL INTEGERS

(test-group "c-dadd1"
  (test 1 (ck () (c-dadd1 '0)))
  (test 8 (ck () (c-dadd1 '7)))
  (test 16 (ck () (c-dadd1 '15)))

  (test #f (ck () (c-dadd1 '-1)))
  (test #f (ck () (c-dadd1 '16)))
  (test #f (ck () (c-dadd1 'foo))))


(test-group "c-dsub1"
  (test 0 (ck () (c-dsub1 '1)))
  (test 8 (ck () (c-dsub1 '9)))
  (test 15 (ck () (c-dsub1 '16)))

  (test #f (ck () (c-dsub1 '0)))
  (test #f (ck () (c-dsub1 '17)))
  (test #f (ck () (c-dsub1 'foo))))


(test-group "c-du"
  (test '() (ck () (c-quote (c-du '0))))
  (test '(0) (ck () (c-quote (c-du '1))))
  (test '(1 0) (ck () (c-quote (c-du '2))))
  (test '(15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0)
        (ck () (c-quote (c-du '16))))
  (test #f (ck () (c-quote (c-du '17))))
  (test #f (ck () (c-quote (c-du '-1))))

  ;; Define %sub1 which has a very small domain.
  (ck () (c-list 'define-syntax '%sub1 (c-make-next '(5 4 3 2 1 0))))

  (test '() (ck () (c-quote (c-du '0 '(%sub1)))))
  (test '(2 1 0) (ck () (c-quote (c-du '3 '(%sub1)))))
  (test '(4 3 2 1 0) (ck () (c-quote (c-du '5 '(%sub1)))))
  (test #f (ck () (c-quote (c-du '6 '(%sub1)))))
  (test #f (ck () (c-quote (c-du '-1 '(%sub1))))))


(test-group "c-ud"
  (test 0 (ck () (c-ud '())))
  (test 1 (ck () (c-ud '(a))))
  (test 16 (ck () (c-ud (c-du '16))))
  (test 16 (ck () (c-ud '(a b c d e f g h i j k l m n o p))))
  (test #f (ck () (c-ud '(a b c d e f g h i j k l m n o p q))))

  ;; Define %add1 which has a very small domain.
  (ck () (c-list 'define-syntax '%add1 (c-make-next '(0 1 2 4 5))))

  (test 0 (ck () (c-ud '() '(%add1))))
  (test 1 (ck () (c-ud '(a) '(%add1))))
  (test 5 (ck () (c-ud '(a b c d) '(%add1))))
  (test #f (ck () (c-ud '(a b c d e) '(%add1)))))


(test-group "c-dlist-ref"
  (test 2 (ck () (c-dlist-ref '(2 7 1 8) '0)))
  (test 7 (ck () (c-dlist-ref '(2 7 1 8) '1)))
  (test 1 (ck () (c-dlist-ref '(2 7 1 8) '2)))
  (test 8 (ck () (c-dlist-ref '(2 7 1 8) '3)))
  (test-error (ck () (c-dlist-ref '() '0)))
  (test-error (ck () (c-dlist-ref '(2 7 1 8) '-1)))
  (test-error (ck () (c-dlist-ref '(2 7 1 8) '-4)))

  ;; Define %sub1 which has a very small domain.
  (ck () (c-list 'define-syntax '%sub1 (c-make-next '(2 1 0))))

  (test 2 (ck () (c-dlist-ref '(2 7 1 8) '0 '(%sub1))))
  (test 7 (ck () (c-dlist-ref '(2 7 1 8) '1 '(%sub1))))
  (test 1 (ck () (c-dlist-ref '(2 7 1 8) '2 '(%sub1))))
  (test-error (ck () (c-dlist-ref '(2 7 1 8) '3 '(%sub1))))

  (test-error (ck () (c-dlist-ref '() '0 '(%sub1))))
  (test-error (ck () (c-dlist-ref '(2 7 1 8) '-1 '(%sub1))))
  (test-error (ck () (c-dlist-ref '(2 7 1 8) '-4 '(%sub1)))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ENVIRONMENT

(test-group "c-env-assoc"
  (test '(a . 1)
        (ck () (c-quote (c-env-assoc 'a '((z . 9) (a . 1))))))
  (test '(z . 9)
        (ck () (c-quote (c-env-assoc 'z '((z . 9) (a . 1))))))
  (test '(0 . "0")
        (ck () (c-quote (c-env-assoc '0 '((16 . "f") (0 . "0"))))))
  (test '(16 . "f")
        (ck () (c-quote (c-env-assoc '16 '((16 . "f") (0 . "0")))))))


(test-group "c-let"
  ;; Empty bindings
  (test '(())
        (ck () (c-quote (c-let '()
                          '(%ck:env-alist)))))

  (test '(((a . 1)))
        (ck () (c-quote (c-let '((a '1))
                          '(%ck:env-alist)))))

  ;; Evaluate an expression
  (test '(((a . #(b c))))
        (ck () (c-quote (c-let '((a (c-vector 'b 'c)))
                          '(%ck:env-alist)))))

  ;; Nested c-let
  (test '(((a . 2) (b . 3))
          ((a . 1)))
        (ck () (c-quote (c-let '((a '1))
                          '(c-let '((a '2) (b '3))
                             '(%ck:env-alist))))))

  (test '(((x . "x2") (z . ("x1" "y")))
          ((x . "x1") (y . "y")))
        (ck () (c-quote (c-let '((x '"x1") (y '"y"))
                          '(c-let '((x '"x2")
                                    (z (c-list (c-var 'x) (c-var 'y))))
                             '(%ck:env-alist))))))

  ;; Variable names don't have to be symbols.
  (test '((("foo" . "bar") (#\a . #\b) ((x (y)) . z) (#(1) . 2)))
        (ck () (c-quote (c-let '(("foo" '"bar")
                                 (#\a '#\b)
                                 ((x (y)) 'z)
                                 (#(1) '2))
                          '(%ck:env-alist))))))


(test-group "c-let*"
  (test '(())
        (ck () (c-quote (c-let* '()
                          '(%ck:env-alist)))))

  (test '(((a . 1)))
        (ck () (c-quote (c-let* '((a '1))
                          '(%ck:env-alist)))))

  (test '(((c . 3))
          ((b . 2))
          ((a . 1)))
        (ck () (c-quote (c-let* '((a '1) (b '2) (c '3))
                          '(%ck:env-alist)))))

  (test '(((z . ("x2" "y")))
          ((x . "x2"))
          ((y . "y"))
          ((x . "x1")))
        (ck () (c-quote (c-let* '((x '"x1") (y '"y"))
                          '(c-let* '((x '"x2")
                                     (z (c-list (c-var 'x) (c-var 'y))))
                             '(%ck:env-alist))))))

  ;; Variable names don't have to be symbols.
  (test '(((#(1) . 2))
          (((x (y)) . z))
          ((#\a . #\b))
          (("foo" . "bar")))
        (ck () (c-quote (c-let* '(("foo" '"bar")
                                  (#\a '#\b)
                                  ((x (y)) 'z)
                                  (#(1) '2))
                          '(%ck:env-alist))))))


(test-group "c-var"
  (test 1 (ck () (c-let '((a '1)) '(c-var 'a))))
  (test-error (ck () (c-let '() '(c-var 'a))))

  (test "inner"
        (ck () (c-let '((0 '"outer"))
                 '(c-let '((0 '"inner"))
                    '(c-var '0)))))

  (test '(1 2 3 4)
        (ck () (c-quote
                (c-let '((a (c-list '1 '2))
                         (b '(4)))
                  '(c-let '((b (c-cons '3 (c-var 'b))))
                     '(c-append (c-var 'a) (c-var 'b)))))))

  ;; An assoc operation that only matches "foo" and "bar".
  (ck () `(define-syntax %foobar
            ,(c-make-assoc '%foobar '() '("foo" "bar"))))

  ;; Not found using c-env-assoc
  (test-error (ck () (c-let '(("foo" '1))
                       '(c-var '"foo"))))
  ;; Found using %foobar
  (test 1 (ck () (c-let '(("foo" '1))
                   '(c-var '"foo" '(%foobar)))))
  (test '(1 2)
        (ck () (c-quote
                (c-let '(("foo" '1))
                  '(c-let '(("bar" '2))
                     '(c-list (c-var '"foo" '(%foobar))
                              (c-var '"bar" '(%foobar))))))))

  ;; Dynamic variable scope
  (define-syntax %read-x
    (syntax-rules (quote)
      ((%read-x s)
       (ck s (c-var 'x)))))
  (define-syntax %bind-then-read-x
    (syntax-rules (quote)
      ((%bind-then-read-x s)
       (ck s (c-let '((x '1)) '(%read-x))))))
  (test 1 (ck () (%bind-then-read-x))))


(test-group "c-var/default"
  (test 1 (ck () (c-let '((a '1)) '(c-var/default 'a ''42))))
  (test 42 (ck () (c-let '() '(c-var/default 'a ''42))))
  (test '(42) (ck () (c-quote
                      (c-let '()
                        '(c-var/default 'a '(c-list '42))))))

  ;; An assoc operation that only matches "foo" and "bar".
  (ck () `(define-syntax %foobar
            ,(c-make-assoc '%foobar '() '("foo" "bar"))))

  ;; Not found using c-env-assoc
  (test -1 (ck () (c-let '(("foo" '1))
                    '(c-var/default '"foo" ''-1))))
  ;; Found using %foobar
  (test 1 (ck () (c-let '(("foo" '1))
                   '(c-var/default '"foo" ''-1 '(%foobar))))))


(test-group "c-var?"
  (test #t (ck () (c-let '((a '1)) '(c-var? 'a))))
  (test #f (ck () (c-let '((a '1)) '(c-var? 'z))))
  (test #f (ck () (c-let '() '(c-var? 'a))))
  (test #t (ck () (c-let '((a '1))
                    '(c-let '()
                       '(c-var? 'a)))))

  (ck () `(define-syntax %foobar
            ,(c-make-assoc '%foobar '() '("foo" "bar"))))
  (test #f (ck () (c-let '(("foo" '1))
                    '(c-var? '"foo"))))
  (test #t (ck () (c-let '(("foo" '1))
                    '(c-var? '"foo" '(%foobar))))))
