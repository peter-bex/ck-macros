;;; Test entry point for CHICKEN Scheme.

(cond-expand
  (chicken-4
   (use ck-macros test)
   (define-for-syntax pseudo-random-integer random))
  (chicken-5
   (import ck-macros test)
   (import-for-syntax (only (chicken random) pseudo-random-integer))))

(test-begin "ck-macros")

(include "test-portable.scm")
(include "test-ck-wrapper.scm")
(include "test-wrappers-r5rs.scm")
(include "test-compare.scm")
(include "test-unfold.scm")
(include "test-c-trace.scm")

(test-end "ck-macros")
(test-exit)
