(test-group "c-unfold"
  (let-syntax ((%c-expt (ck-wrapper expt)))
    (test '(2 4 8 16 32 64 128 256 512 1024)
          (ck () (c-quote
                  (c-unfold '(c-< '10)
                            '(%c-expt '2)
                            '(c-+ '1)
                            '1)))))

  (test
   "10! 9! 8! 7! 6! 5! 4! 3! 2! 1! BLASTOFF!"
   (ck () (c-apply
           '(c-string-append)
           (c-unfold '(c-> '1)
                     '(c-rcompose '((c-number->string)
                                    (c-flip '(c-string-append) '"! ")))
                     '(c-flip '(c--) '1)
                     '10
                     '(c-constantly '("BLASTOFF!")))))))
