(test-group "c-compare?"
  (test #t (ck () (c-compare? '(c-eq?) 'a 'a)))
  (test #f (ck () (c-compare? '(c-eq?) 'a 'x)))
  (test #t (ck () (c-compare? '(c-eq?) '(a . b) '(a . b))))
  (test #f (ck () (c-compare? '(c-eq?) '(a . b) '(a . x))))
  (test #f (ck () (c-compare? '(c-eq?) '(a . b) '(a b))))
  (test #f (ck () (c-compare? '(c-eq?) '(a . b) '#(a b))))
  (test #f (ck () (c-compare? '(c-eq?) '(a b) '#(a b))))
  (test #t (ck () (c-compare? '(c-eq?) '(a (b)) '(a (b)))))
  (test #f (ck () (c-compare? '(c-eq?) '(a (b)) '(a (x)))))
  (test #f (ck () (c-compare? '(c-eq?) '(a (b)) '(a #(b)))))
  (test #f (ck () (c-compare? '(c-eq?) '#(a (b)) '(a (b)))))

  (test #t (ck () (c-compare? '(c-true) '(1 #(2) 3) '(a #(b) c))))
  (test #f (ck () (c-compare? '(c-true) '(1 #(2) 3) '(1 #(2) 3 4))))

  (test #t (ck () (c-compare? '(c-string-ci=?) '("a" ("B")) '("A" ("b")))))
  (test #f (ck () (c-compare? '(c-string-ci=?) '("a" ("B")) '#("A" ("b")))))
  (test #f (ck () (c-compare? '(c-string-ci=?) '#("a" ("B")) '("A" ("b")))))

  (test #t (ck () (c-compare? '(c-<) '1 '2)))
  (test #f (ck () (c-compare? '(c-<) '2 '1)))
  (test #t (ck () (c-compare? '(c-<) '(1 #(5)) '(2 #(6)))))
  (test #f (ck () (c-compare? '(c-<) '(1 #(5)) '(2 (6)))))

  ;; Predicate can return any non-#f value, the result is still '#t.
  (let-syntax ((pred (ck-wrapper (lambda (x y) (if (= x y) "yay" #f)))))
    (test #t (ck () (c-compare? '(pred) '1 '1)))
    (test #f (ck () (c-compare? '(pred) '1 '2)))
    (test #t (ck () (c-compare? '(pred) '(1 2 3) '(1 2 3))))
    (test #f (ck () (c-compare? '(pred) '(1 2 3) '(1 2 4))))))
