
;;; ck-macros
;;; Composable Scheme macros based on the CK abstract machine.
;;; Version 0.4.0 (in development)
;;;
;;; All source code (including contributions) is released by its
;;; authors to the public domain. For more information, please refer
;;; to <http://unlicense.org>
;;;
;;; `c-error' is credit John Croisant.
;;;
;;; This code is portable to any R5RS Scheme. It will cause a syntax
;;; error by deliberately calling a non-matching form of itself.


;;; (c-error MESSAGE OBJ ...)
;;;
;;; Raises a syntax error in an implementation-specific way. The error
;;; will include the user-provided message, and optional objects. The
;;; message and objects will be evaluated. The error will also include
;;; the CK stack, in a form meant for humans to read.
(define-syntax c-error
  (syntax-rules (quote)
    ((c-error (("K" . (((call evaled ...) unevaled ...) ...))
               ("E" . E))
              'message 'obj ...)
     (c-error message 'obj ...
              "call stack:" ((call evaled ... ^^^ unevaled ...) ...)
              "environment:" E))))
